const int buttonPin = 2;
const int rLED = 7;
const int yLED = 6;
const int gLED = 5;
int buttonState = 0;
int counter = 0;
int counterAuto = 0;
unsigned long previousMillis = 0;
const long interval = 250;

void initLED() {
  if (counter <= 2) {
    manMode(counter);
  } else if (counter == 3) {
    digitalWrite(rLED, HIGH);
    digitalWrite(gLED, HIGH);
    digitalWrite(yLED, HIGH);
  } else if (counter == 4) {
    digitalWrite(rLED, LOW);
    digitalWrite(gLED, LOW);
    digitalWrite(yLED, LOW);
  } else if (counter == 5) {
    digitalWrite(LED_BUILTIN, LOW);
    autoMode();
  } else {
    digitalWrite(rLED, LOW);
    digitalWrite(gLED, LOW);
    digitalWrite(yLED, LOW);
  }
}

void manMode(int c) {
  if (c == 0) {
    digitalWrite(gLED, HIGH);
    digitalWrite(rLED, LOW);
    digitalWrite(yLED, LOW);
  } else if (c == 1) {
    digitalWrite(yLED, HIGH);
    digitalWrite(gLED, LOW);
    digitalWrite(rLED, LOW);
  } else if (c == 2) {
    digitalWrite(rLED, HIGH);
    digitalWrite(gLED, LOW);
    digitalWrite(yLED, LOW);
  }
}

void autoMode() {
  manMode(counterAuto);
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    if (counterAuto < 2)
      counterAuto++;
    else
      counterAuto = 0;
  }
}

void setup() {
  pinMode(rLED, OUTPUT);
  pinMode(yLED, OUTPUT);
  pinMode(gLED, OUTPUT);
  pinMode(buttonPin, INPUT);
  counter = 0;
}

void loop() {
  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {
    while (buttonState == HIGH) {
      initLED();
      buttonState = digitalRead(buttonPin);
    }

    if (counter < 5)
      counter++;
    else
      counter = 0;
  }

  initLED();
}
